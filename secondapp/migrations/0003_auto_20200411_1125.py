# Generated by Django 3.0.4 on 2020-04-11 05:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('secondapp', '0002_auto_20200405_1135'),
    ]

    operations = [
        migrations.CreateModel(
            name='category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('cover_pic', models.FileField(upload_to='media/%y/%m/%d')),
                ('description', models.TextField()),
                ('added_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AlterModelOptions(
            name='contact_us',
            options={'verbose_name_plural': 'contact_us'},
        ),
        migrations.AlterModelOptions(
            name='student',
            options={'verbose_name_plural': 'student'},
        ),
    ]
