# Generated by Django 3.0.4 on 2020-04-05 06:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('secondapp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='contact_us',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('contact_number', models.IntegerField(blank=True, unique=True)),
                ('subject', models.CharField(max_length=250)),
                ('message', models.TextField()),
                ('added_on', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.AlterField(
            model_name='student',
            name='address',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='student',
            name='fee',
            field=models.FloatField(blank=True),
        ),
    ]
