from django.contrib import admin
from secondapp.models import student,contact_us,category,register_table, add_product,cart,Order

admin.site.site_header="My Website"
class studentAdmin(admin.ModelAdmin):
    # fields = ["r_no","email","name"]
    list_display = ["name","r_no","email","fee","gender","address","is_registered"]
    search_fields = ["r_no","name"]
    list_filter = ["name","gender","r_no"]
    list_editable = ["email"]

class contact_usAdmin(admin.ModelAdmin):
    #fields = ["contact_number","name"]
    fields = ["contact_number","name","subject","message"]
    list_display = ["id","name","contact_number","subject","message","added_on"]
    search_fields = ["name"]
    list_filter = ["name","added_on"]
    list_editable = ["name"] 

class categoryAdmin(admin.ModelAdmin):
    # fields=["name","cover_pic","description"]
    list_display = ["id","name","description","added_on"]    

admin.site.register(student,studentAdmin)
admin.site.register(contact_us,contact_usAdmin)
admin.site.register(category,categoryAdmin)
admin.site.register(register_table)
admin.site.register(add_product)
admin.site.register(cart)
admin.site.register(Order)
