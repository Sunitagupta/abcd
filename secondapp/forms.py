from django import forms
from secondapp.models import add_product


class add_product_form(forms.ModelForm):
    class Meta:
        model = add_product
        # fields = "__all__"  #agr humee models ki all fiels chahiye
        fields = ["product_name","product_category","product_price","sale_price","product_image","details"]  
        #  hume konsi fields chahiye
        # exclude = ["product_name"]  # jabb hume koi 1-2 fields model me nhi chahiye ush fields ka name likh doo